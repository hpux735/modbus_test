use tokio_modbus::prelude::*;
use tokio_serial::SerialStream;
use pretty_env_logger;
use color_eyre::{Result, eyre::eyre};
use byte_slice_cast::AsByteSlice;
use std::str;
use tokio_modbus::client::Context;
use num_traits::FromPrimitive;
use num_derive::FromPrimitive;

#[derive(Debug, FromPrimitive)]
enum ChargeState {
    Deactivated   = 0x00,
    Activated     = 0x01,
    MPPT          = 0x02,
    Equalizing    = 0x03,
    Boost         = 0x04,
    Floating      = 0x05,
    CurrentLimit  = 0x06,
}

async fn product_id(context: &mut Context) -> Result<String> {
    // Get the product ID
    let rsp = context.read_holding_registers(0x000C, 0x0008).await?;

    // Convert it from u16 to u8
    let rsp = rsp.as_slice().as_byte_slice();
    let product_id = str::from_utf8(rsp)?.to_string();

    Ok(product_id)
}

async fn software_version(context: &mut Context) -> Result<(u16, u16)> {
    let rsp = context.read_holding_registers(0x0014, 0x0002).await?;
    Ok((rsp[0], rsp[1]))
}

async fn hardware_version(context: &mut Context) -> Result<(u16, u16)> {
    let rsp = context.read_holding_registers(0x0016, 0x0002).await?;
    Ok((rsp[0], rsp[1]))
}

async fn serial_number(context: &mut Context) -> Result<u32> {
    let rsp = context.read_holding_registers(0x0018, 0x0002).await?;
    let retval: u32 = ((rsp[0] as u32) << 16) + rsp[1] as u32;
    Ok(retval)
}

async fn battery_soc(context: &mut Context) -> Result<f32> {
    let rsp = context.read_holding_registers(0x0100, 0x0001).await?;
    Ok(rsp[0] as f32)
}

async fn battery_voltage(context: &mut Context) -> Result<f32> {
    let rsp = context.read_holding_registers(0x0101, 0x0001).await?;
    Ok((rsp[0] as f32) / 10.0)
}

async fn charge_current(context: &mut Context) -> Result<f32> {
    let rsp = context.read_holding_registers(0x0102, 0x0001).await?;
    Ok((rsp[0] as f32) / 100.0)
}

async fn charge_state(context: &mut Context) -> Result<ChargeState> {
    let rsp = context.read_holding_registers(0x0120, 0x0001).await?;
    let value = (rsp[0] & 0xFF).try_into()?;
    let state: Option<ChargeState> = FromPrimitive::from_i8(value);
    match state {
        None => Err(eyre!("Unable to convert {} into a Charge State.", value)),
        Some(val) => Ok(val)
    }
}

async fn charge_power(context: &mut Context) -> Result<u16> {
    let rsp = context.read_holding_registers(0x0109, 0x0001).await?;
    Ok(rsp[0])
}
async fn load_voltage(context: &mut Context) -> Result<f32> {
    let rsp = context.read_holding_registers(0x0104, 0x0001).await?;
    Ok((rsp[0] as f32) / 10.0)
}

async fn load_current(context: &mut Context) -> Result<f32> {
    let rsp = context.read_holding_registers(0x0105, 0x0001).await?;
    Ok((rsp[0] as f32) / 100.0)
}

async fn load_power(context: &mut Context) -> Result<u16> {
    let rsp = context.read_holding_registers(0x0106, 0x0001).await?;
    Ok(rsp[0])
}

async fn solar_voltage(context: &mut Context) -> Result<f32> {
    let rsp = context.read_holding_registers(0x0107, 0x0001).await?;
    Ok((rsp[0] as f32) / 10.0)
}

async fn solar_current(context: &mut Context) -> Result<f32> {
    let rsp = context.read_holding_registers(0x0108, 0x0001).await?;
    Ok((rsp[0] as f32) / 100.0)
}


#[tokio::main(flavor = "current_thread")]
pub async fn main() -> Result<()> {
    pretty_env_logger::init();

    let tty_path = "/dev/ttyUSB1";
    let slave = Slave(0x10);

    let builder = tokio_serial::new(tty_path, 9600);
    let port = SerialStream::open(&builder).unwrap();

    let mut ctx = rtu::connect_slave(port, slave).await?;
    println!("Connected to device");

    let product_id = product_id(&mut ctx).await?;
    let software_version = software_version(&mut ctx).await?;
    let hardware_version = hardware_version(&mut ctx).await?;
    let serial_number = serial_number(&mut ctx).await?;
    let battery_soc = battery_soc(&mut ctx).await?;
    let battery_voltage = battery_voltage(&mut ctx).await?;
    let charge_current = charge_current(&mut ctx).await?;
    let charge_power = charge_power(&mut ctx).await?;
    let charge_state = charge_state(&mut ctx).await?;
    let load_voltage = load_voltage(&mut ctx).await?;
    let load_current = load_current(&mut ctx).await?;
    let load_power = load_power(&mut ctx).await?;
    let solar_voltage = solar_voltage(&mut ctx).await?;
    let solar_current = solar_current(&mut ctx).await?;

    println!("Product ID: {:?}", product_id);
    println!("Software version: {}.{}", software_version.0, software_version.1);
    println!("Hardware version: {}.{}", hardware_version.0, hardware_version.1);
    println!("Serial number: {}", serial_number);
    println!("Battery SOC: {}%", battery_soc);
    println!("Battery voltage: {}V", battery_voltage);
    println!("Charge current: {}A", charge_current);
    println!("Charge power: {}W", charge_power);
    println!("Charge state: {:?}", charge_state);
    println!("Load voltage: {}V", load_voltage);
    println!("Load current: {}A", load_current);
    println!("Load power: {}W ({}W by math)", load_power, load_voltage * load_current);
    println!("Solar voltage: {}V", solar_voltage);
    println!("Solar current: {}A", solar_current);

    Ok(())
}
